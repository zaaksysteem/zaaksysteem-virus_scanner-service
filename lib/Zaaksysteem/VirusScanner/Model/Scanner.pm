package Zaaksysteem::VirusScanner::Model::Scanner;

=head1 NAME

Zaaksysteem::VirusScanner::Model::Scanner - scan for viruses at any URI location

=head1 SYNOPSIS

    my $scanner_client = ClamAV->new();
    my $ua = LWP::UserAgent->new();
    
    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        scanner_client => $scan_client,
        user_agent     => $ua
    );
    
    my $uri = URI->new('http://www.eicar.org/download/eicar.com.txt');
    @viruses = $virus_scanner->scan_uri($uri);

=head1 DESCRIPTION

=cut

use Moose;
use namespace::autoclean;

use BTTW::Tools;
use BTTW::Tools::UA;

use File::Temp;
use HTTP::Status;
use Net::ClamAV::Client;

=head1 ATTRIBUTES

=cut

=head2 scanner

A L<File::Scan::ClamAV> that connects to a C<clamd> deamon. If not supplied, it
will instantiate one using C<CLAMAV_SERVER> and C<CLAMAV_PORT> environment
variables or their default values C<localhost> and port C<3310>.

The object MUST implement the methods:

=over

=item ping

to check if the clamd can be connected and is alive

=item scan_stream

that takes a C<FH> and scans block by block

=back

=cut

# scanner_client
#
# TODO: make scanner_client plugable
#
# it would be better if the object we pass in had only the requirement that it
# implements two methods
# - ping
# - scan
#
has scanner_client => (
    is => 'ro',
    lazy => 1,
    builder => '_build_scanner_client',
);

sub _build_scanner_client {
    my $host = $ENV{CLAMAV_SERVER} // 'localhost';
    my $port = $ENV{CLAMAV_PORT} // '3310';

    my $url = "$host:$port";

    return Net::ClamAV::Client->new(
        url => $url,
    );
}

=head2 user_agent

A L<UserAgent> that can take a L<URI> object and store the content of the
response to a given filename.

=cut

has user_agent => (
    is => 'ro',
    lazy => 1,
    builder => '_build_user_agent',
);

sub _build_user_agent {
    return BTTW::Tools::UA::new_user_agent(
        protocols_allowed => ['http', 'https']
    )
}

=head1 METHODS

=cut

=head2 scan_uri

    $virus = $virus_scanner->scan_uri($uri);

Scans on a location given by a L<URI> object for virusses and returns a list of
viruses found or an empty list when clean. Some virus scanners will only report
the first virus encountered.

=cut

sig scan_uri => 'URI';

sub scan_uri {
    my ($self, $uri) = @_;

    $self->scanner_client->ping();

    my $temp_file = $self->_file_temp_from_uri($uri);

    # Net::ClamAV::Client's "is this a handle" check is wrong; so work around it
    # by making it re-open the file itself.
    my $virus = $self->scanner_client->scanStreamFile("$temp_file");
    # ClamAV only retuns the first virus

    # we will always return a list, empty when there are no viruses found
    return () if not defined $virus;
    return ( $virus );
}

# _file_temp_from_uri
#
#.  my $tmp = $self->_file_temp_from_uri($uri);
#
# downloads data from $uri and stores it in a File::Temp.
#
# remember, File::Temp acts as a FH and auto stringifies, see the documentation
#
sub _file_temp_from_uri {
    my ($self, $uri) = @_;

    my $temp_file = File::Temp->new();

    my $response = $self->user_agent->get($uri->as_string(),
        ':content_file' => $temp_file->filename()
    );

    return $temp_file if $response->is_success;

    my $status_code = $response->code();
    my $status_message = HTTP::Status::status_message($status_code);
    $status_message =~ tr/A-Z \-'/a-z___/;

    throw(
        'virus_scanner_service/scanner/http_get_not_successful',
        sprintf("Virus Scan Service can not get file from: [%s - http status: %s]",
            $uri->as_string(),
            $status_code,
        ),
        { http_code => $status_code },
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
